# Wallpapers
My wallpaper repository.

![](Resources/screenshot.jpg "Wallpapers")

## Credits
Credits go to the creators of the wallpapers.

The `fedora26` and `fedora34` wallpapers are released by the Fedora Project (respectively Fedora 26 and Fedora 34 releases). The `brushes` and `adwaita42` wallpapers where part of the releas of GNOME 42. The `flow` wallpapers are created and released by KDE.

